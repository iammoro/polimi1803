################
#### HELPER ####
################
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:polimi1803/Configuration/TypoScript/Helpers">


plugin.tx_polimi1803 {
  view {
    templateRootPaths.0 = EXT:polimi1803/Resources/Private/Templates/
    templateRootPaths.1 = {$plugin.tx_polimi1803.view.templateRootPath}
    partialRootPaths.0 = EXT:polimi1803/Resources/Private/Partials/
    partialRootPaths.1 = {$plugin.tx_polimi1803.view.partialRootPath}
    layoutRootPaths.0 = EXT:polimi1803/Resources/Private/Layouts/
    layoutRootPaths.1 = {$plugin.tx_polimi1803.view.layoutRootPath}
  }
}

#############################
#### DEBUG SETTINGS      ####
#############################
[applicationContext = Development*]
config {
  sendCacheHeaders = 0
  contentObjectExceptionHandler = 0
  no_cache                            = 1
  debug                               = 1
  admPanel                            = 1
  #config.tx_realurl_enable = 0

  // Compression and Concatenation of CSS and JS Files
    compressJs                          = 0
    compressCss                         = 0
    concatenateJs                       = 0
    concatenateCss                      = 0

}
[global]




