styles.templates {
    templateRootPath = EXT:polimi1803/Resources/Private/Extensions/fluid_styled_content/Templates/
    partialRootPath = EXT:polimi1803/Resources/Private/Extensions/fluid_styled_content/Partials/
    layoutRootPath = EXT:polimi1803/Resources/Private/Extensions/fluid_styled_content/Layouts/
}
