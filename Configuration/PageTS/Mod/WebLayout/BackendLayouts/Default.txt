################################
#### BACKENDLAYOUT: DEFAULT ####
################################
mod {
    web_layout {
        BackendLayouts {
            default {
                title = LLL:EXT:polimi1803/Resources/Private/Language/Backend.xlf:backend_layout.default
                config {
                    backend_layout {
                        colCount = 6
                        rowCount = 2
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = LLL:EXT:polimi1803/Resources/Private/Language/Backend.xlf:backend_layout.column.normal
                                        colPos = 0
                                        colspan = 6
                                    }
                                }
                            }
                            2 {
                                columns {
                                    1 {
                                        name = LLL:EXT:polimi1803/Resources/Private/Language/Backend.xlf:backend_layout.column.footer.left
                                        colPos = 10
                                        colspan = 4
                                    }
                                    2 {
                                        name = LLL:EXT:polimi1803/Resources/Private/Language/Backend.xlf:backend_layout.column.footer.right
                                        colPos = 12
                                        colspan = 2
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:polimi1803/Resources/Public/Images/BackendLayouts/default.gif
            }
        }
    }
}
