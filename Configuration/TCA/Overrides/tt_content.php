<?php
defined('TYPO3_MODE') or die();

call_user_func(function () {
    $languageFilePrefix = 'LLL:EXT:fluid_styled_content/Resources/Private/Language/Database.xlf:';
    $frontendLanguageFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';

    $LL = 'LLL:EXT:polimi1803/Resources/Private/Language/Backend.xlf:';


    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            $LL. 'wizard.title',
            'fs_slider',
            'content-image'
        ],
        'textmedia',
        'after'
    );

    $GLOBALS['TCA']['tt_content']['types']['fs_slider'] = [
        'showitem'         =>
                '--palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,'
                . '--palette--;' . $languageFilePrefix . 'tt_content.palette.mediaAdjustments;mediaAdjustments,'
                . 'pi_flexform,'
                . '--div--;' . $LL . 'tca.tab.sliderElements,'
                . 'assets'
    ];    

});
