<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}


// included PageTS for polimi1803
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/PageTS/All.txt">');

